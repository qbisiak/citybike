package citybike.config;

import java.util.Properties;

import javax.sql.DataSource;

import liquibase.integration.spring.SpringLiquibase;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
//import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
//import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;

@Configuration
@ComponentScan(basePackages = "citybike") 
@Import({ThymeleafConfig.class, AppInitializer.class, ServiceConfig.class, WebMvcConfig.class})
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@ImportResource("classpath:spring-security.xml") 
class AppConfig {
	
	private static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
    private static final String PROPERTY_NAME_DATABASE_URL = "db.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";
      
    private static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
    private static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";
    private static final String PROPERTY_NAME_HIBERNATE_AUTO_CREATE = "hibernate.hbm2ddl.auto";
    private static final String PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN = "entitymanager.packages.to.scan";
    
    @Autowired
    private Environment env;  
    
    @Bean
    public DataSource dataSource() {  
        DriverManagerDataSource dataSource = new DriverManagerDataSource();  
          
        dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));  
        dataSource.setUrl(env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));  
        dataSource.setUsername(env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));  
        dataSource.setPassword(env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));  
          
        return dataSource;  
    }  
      
    @Bean  
    public LocalSessionFactoryBean sessionFactory() {  
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();  
        sessionFactoryBean.setDataSource(dataSource());  
        sessionFactoryBean.setPackagesToScan(env.getRequiredProperty(PROPERTY_NAME_ENTITYMANAGER_PACKAGES_TO_SCAN));  
        sessionFactoryBean.setHibernateProperties(hibProperties());  
        return sessionFactoryBean;  
    }  
      
    private Properties hibProperties() {  
        Properties properties = new Properties();  
        properties.put(PROPERTY_NAME_HIBERNATE_DIALECT, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_DIALECT));  
        properties.put(PROPERTY_NAME_HIBERNATE_SHOW_SQL, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_SHOW_SQL));
        properties.put(PROPERTY_NAME_HIBERNATE_AUTO_CREATE, env.getRequiredProperty(PROPERTY_NAME_HIBERNATE_AUTO_CREATE));
        return properties;    
    }  
      
    @Bean  
    public HibernateTransactionManager transactionManager() {  
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();  
        transactionManager.setSessionFactory(sessionFactory().getObject());  
        return transactionManager;  
    }
    
    @Bean  
    public SpringLiquibase getLiquibase() {  
        final SpringLiquibase liquibase = new SpringLiquibase();  
        liquibase.setDataSource(dataSource());
        liquibase.setChangeLog("classpath:liquibase/db.changelog-master.sql");
        return liquibase;  
    }
    
    @Bean
    public PasswordEncoder passwordEncoder(){
    	return new Md5PasswordEncoder();
    }
    
}