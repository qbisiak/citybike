package citybike.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import citybike.model.Role;
import citybike.model.Roles;
import citybike.model.User;
//import org.springframework.security.authentication.encoding.PasswordEncoder;

@Repository
public class UserDaoImpl implements UserDao {
 
	@Autowired
	private SessionFactory sessionFactory;
 
	@Override
	@SuppressWarnings("unchecked")
	public User findByUserName(String username) {
 
		List<User> users = new ArrayList<User>();
		users = sessionFactory.getCurrentSession()
			.createQuery("from User where username=:username")
			.setParameter("username", username)
			.list();
 
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}
 
	}
	
	

	@Override
	public List<User> getAll() {
		return sessionFactory.getCurrentSession()
				.createQuery("from User")
				.list();
	}

	

	@Override
	public List<Role> getAllRoles() {
		return sessionFactory.getCurrentSession()
				.createQuery("from Role")
				.list();
	}



	@Override
	public void add(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.save(user);
/*		for (UserRole role : user.getUserRole())
		{
			session.save(role);
		}*/
	}
	
	

	@Override
	public void disable(String username) {
		sessionFactory.getCurrentSession()
			.createQuery("update User set enabled = false where username = :username")
			.setParameter("username", username)
			.executeUpdate();

		
	}



	@Override
	public List<User> findUsersByRole(String role) {
		return sessionFactory.getCurrentSession()
		.createQuery("select u from User u join u.roles r where r.role = :role")
		.setParameter("role", role)
		.list();
	}

	

	@Override
	public List<User> findEmployees() {
		return sessionFactory.getCurrentSession()
		.createQuery("select distinct u from User u join u.roles r where r.role = :admin or r.role = :emp or r.role = :accountant")
		.setParameter("admin", Roles.ADMIN.getRole())
		.setParameter("emp", Roles.EMPLOYEE.getRole())
		.setParameter("accountant", Roles.ACCOUNTANT.getRole())
		.list();
	}



	@Override
	public void update(User user) {
		sessionFactory.getCurrentSession()
			.update(user);
		
	}

	
	
}