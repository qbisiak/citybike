package citybike.dao;

import java.util.List;

import org.joda.time.LocalDateTime;

import citybike.model.Bike;
import citybike.model.BikeTransfer;
import citybike.model.User;

public interface BikeDao {

	public Bike findByName(String name);
	public Bike findById(Long id);
	public void update(Bike bike);
	public void saveTransfer(BikeTransfer bikeTransfer);
	public BikeTransfer lastTransfer(Long bikeId, String toUsername);
	public void clearTransfers();
	public List<BikeTransfer> getTransfers(String username, LocalDateTime after);
	public List<Bike> getAll();
	public Integer countEnabled();
	public Integer countLocked();
}
