package citybike.dao;

import citybike.model.Payment;

public interface PaymentDao {
	
	public void add(Payment payment);
	
}
