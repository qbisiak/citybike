package citybike.dao;

import java.util.List;

import citybike.model.Role;
import citybike.model.User;

public interface UserDao {
	 
	public User findByUserName(String username);
	public List<User> getAll();
	public List<Role> getAllRoles();
	public void add(User user);
	public void disable(String username);	
	public List<User> findUsersByRole(String role);
	public List<User> findEmployees();
	public void update(User user);
	
}