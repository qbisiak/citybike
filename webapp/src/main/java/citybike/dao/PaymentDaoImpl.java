package citybike.dao;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import citybike.model.Payment;

@Repository
public class PaymentDaoImpl implements PaymentDao {
	
	@Autowired
	SessionFactory sessionFactory;

	@Override
	public void add(Payment payment) {
		
		sessionFactory.getCurrentSession()
			.save(payment);
		
	}
	
	
}
