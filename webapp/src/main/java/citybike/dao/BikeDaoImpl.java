package citybike.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import citybike.model.Bike;
import citybike.model.BikeTransfer;
import citybike.model.User;

@Repository
public class BikeDaoImpl implements BikeDao {

	@Autowired
	SessionFactory sessionFactory;
	
	public Bike findByName(String name) {
		return (Bike)sessionFactory.getCurrentSession()
			.createQuery("from Bike where name = :name")
			.setParameter("name", name)
			.uniqueResult();
	}


	@Override
	public Bike findById(Long id) {
		return (Bike)sessionFactory.getCurrentSession()
				.createQuery("from Bike where id = :id")
				.setParameter("id", id)
				.uniqueResult();
	}
	
	



	@Override
	public Integer countEnabled() {
		return ((Long)sessionFactory.getCurrentSession()
				.createQuery("select count(*) from Bike where enabled = true")
				.uniqueResult()).intValue();
	}
	
	


	@Override
	public Integer countLocked() {
		return ((Long)sessionFactory.getCurrentSession()
				.createQuery("select count(*) from Bike where enabled = true and blocked = true")
				.uniqueResult()).intValue();
	}


	@Override
	public List<BikeTransfer> getTransfers(String username, LocalDateTime after) {
		return sessionFactory.getCurrentSession()
				.createQuery("select * from bike_transfers where (from_username = :username or to_username = :username) and timestamp > :after")
				.setParameter("username", username)
				.setParameter("after", after)
				.list();
	}


	@Override
	public List<Bike> getAll() {
		return sessionFactory.getCurrentSession()
			.createQuery("from Bike where enabled = true")
			.list();
		
	}


	@Override
	public void clearTransfers() {
		sessionFactory.getCurrentSession()
			.createQuery("delete from BikeTransfer")
			.executeUpdate();
	}


	@Override
	public void update(Bike bike) {
		sessionFactory.getCurrentSession()
			.update(bike);
		
	}

	@Override
	public void saveTransfer(BikeTransfer bikeTransfer) {
		sessionFactory.getCurrentSession()
		.save(bikeTransfer);
		
	}
	
	


	@Override
	public BikeTransfer lastTransfer(Long bikeId, String toUsername) {
		String query = "from BikeTransfer where to_username = :user and bike_id = :bike order by timestamp desc";
		
		return (BikeTransfer) sessionFactory.getCurrentSession()
			.createQuery(query)
			.setParameter("bike", bikeId)
			.setParameter("user", toUsername)
			.setMaxResults(1)
			.uniqueResult();
	}




	
	
}
