package citybike.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.joda.money.format.MoneyFormatException;
import org.joda.time.LocalDateTime;

import citybike.view.NewUserView;
import citybike.view.RegisterUserView;

 
@Entity
@Table(name = "users")
public class User {
 
	private String username;
	private String password;
	private boolean enabled = true;
	private boolean blocked = false;
	//private Money balance = Money.zero(CurrencyUnit.EUR);
	private BigDecimal balance = new BigDecimal("0.00");
	private LocalDateTime addedOn = new LocalDateTime();
	
	private Set<Role> roles = new HashSet<Role>();
	private UserDetails details;
	private LocationDetails locationDetails;
	private Set<AlternativeAccess> alternativeAccesses = new HashSet<AlternativeAccess>();
	private Set<Payment> payments = new HashSet<Payment>();
	private Set<BikeTransfer> loans = new HashSet<BikeTransfer>();
	private Set<BikeTransfer> returns = new HashSet<BikeTransfer>();
	private Set<Bike> bikes = new HashSet<Bike>();
 
	public User() {
	}
 
	public User(String username, String password, boolean enabled) {
		this.username = username;
		this.password = password;
		this.enabled = enabled;
	}
 

	
	public User(NewUserView userView)
	{
		this(userView.getUsername(), userView.getPassword(), true);
		this.blocked = false;
		this.addedOn = new LocalDateTime();
		this.balance = new BigDecimal("0.00");
		
		Set<String> rolesStr = userView.getRoles();
		if (rolesStr != null)
			for (String roleStr : rolesStr)
			{
				this.roles.add(new Role(roleStr, this.username));
			}
	}
	
	public User(RegisterUserView userView)
	{
		this(userView.getUsername(), userView.getPassword(), true);
		this.blocked = false;
		this.addedOn = new LocalDateTime();
		this.setDetails(userView.getDetails());
	}

	@Id
	@Column(unique = true, 
		nullable = false, length = 45)
	public String getUsername() {
		return this.username;
	}
 
	public void setUsername(String username) {
		this.username = username;
	}
 
	@Column( 
		nullable = false, length = 60)
	public String getPassword() {
		return this.password;
	}
 
	public void setPassword(String password) {
		this.password = password;
	}
 
	public boolean isEnabled() {
		return this.enabled;
	}
 
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
 
	@ManyToMany(fetch = FetchType.LAZY)
	@Cascade({CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinTable(name="user_roles",
                joinColumns={@JoinColumn(name="username", nullable = false)},
                inverseJoinColumns={@JoinColumn(name="role", nullable = false)})
	public Set<Role> getRoles() {
		return this.roles;
	}
 
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	
	@OneToOne(mappedBy="user", fetch = FetchType.LAZY)
	@Cascade(CascadeType.ALL)
	public UserDetails getDetails() {
		return details;
	}

	public void setDetails(UserDetails details) {
		this.details = details;
	}
	
	
	@OneToOne(mappedBy="username", fetch = FetchType.LAZY)
	public LocationDetails getLocationDetails() {
		return locationDetails;
	}

	public void setLocationDetails(LocationDetails locationDetails) {
		this.locationDetails = locationDetails;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	
	
	@Column(nullable = false)
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	/*
	@Column(name = "balance", nullable = false)
	@Type(type="org.jadira.usertype.moneyandcurrency.joda.PersistentMoneyAmount", parameters = { @Parameter(name="currencyCode", value="EUR") })
	public Money getBalance() {
		return balance;
	}

	public void setBalance(Money balance) {
		this.balance = balance;
	}
	*/
	@Column(name = "added_on")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	public LocalDateTime getAddedOn() {
		return addedOn;
	}

	public void setAddedOn(LocalDateTime addedOn) {
		this.addedOn = addedOn;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<AlternativeAccess> getAlternativeAccesses() {
		return alternativeAccesses;
	}

	public void setAlternativeAccesses(Set<AlternativeAccess> alternativeAccesses) {
		this.alternativeAccesses = alternativeAccesses;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Payment> getPayments() {
		return payments;
	}

	public void setPayments(Set<Payment> payments) {
		this.payments = payments;
	}
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "toUser")
	public Set<BikeTransfer> getLoans() {
		return loans;
	}

	public void setLoans(Set<BikeTransfer> loans) {
		this.loans = loans;
	}
	
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "fromUser")
	public Set<BikeTransfer> getReturns() {
		return returns;
	}

	public void setReturns(Set<BikeTransfer> returns) {
		this.returns = returns;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	public Set<Bike> getBikes() {
		return bikes;
	}

	public void setBikes(Set<Bike> bikes) {
		this.bikes = bikes;
	}
	
	
 
}
