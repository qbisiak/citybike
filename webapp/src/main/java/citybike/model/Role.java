package citybike.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;



@Entity
@Table(name = "roles")
public class Role {
	
	private String role;
	private String name;
	
	private Set<User> users = new HashSet<User>();
	
	public Role(){}
	
	public Role(String role, String name) {
		super();
		this.role = role;
		this.name = name;
	}

	@Id
	@Column(name = "role", unique = true, 
		nullable = false, length = 45)
	public String getRole() {
		return role;
	}
	
	@Column(name = "name", nullable = false, length = 45)
	public void setRole(String role) {
		this.role = role;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "roles")
	@Cascade({CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
	public Set<User> getUsers() {
		return this.users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}

	
}
