package citybike.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "user_details")
public class UserDetails {
	
	private User user;
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String telephone;
	private LocalDate brithDate;
	private String address;
	private String zipCode;
	private String city = "Łódź";
	private String country = "Polska";
	
	
	public UserDetails(){}	
	
	public UserDetails(User user, Long id, String firstName, String lastName,
			String email, String telephone, LocalDate brithDate,
			String address, String zipCode, String city, String country) {
		super();
		this.user = user;
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.telephone = telephone;
		this.brithDate = brithDate;
		this.address = address;
		this.zipCode = zipCode;
		this.city = city;
		this.country = country;
	}

	
	@OneToOne
	@JoinColumn(name = "username")
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 35)
	@Size(min = 2, max = 35, message="imię musi się składać z {min} - {max} znaków")
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(length = 70)
	@Size(min = 2, max = 70, message="nazwisko musi się składać z {min} - {max} znaków")
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Email(message="nieprawidłowy adres email")
	@NotEmpty(message="pole nie może być puste")
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(length = 20)
	@Pattern(regexp="(^[0-9]{9,11}$)", message="nieprawidłowy numer")
	public String getTelephone() {
		return telephone;
	}
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
	@NotNull(message = "wprowadź datę urodzenia")
	@DateTimeFormat(pattern="dd-MM-yyyy")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
	public LocalDate getBrithDate() {
		return brithDate;
	}
	public void setBrithDate(LocalDate brithDate) {
		this.brithDate = brithDate;
	}
	
	@Column
	@NotEmpty(message = "wprowadź adres")
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(length = 8)
	@Pattern(regexp = "^[0-9]{2}\\-[0-9]{3}$",message = "wprowadz kod pocztowy w formie 00-000")
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	@Column(length = 50)
	@NotEmpty(message = "wprowadź miasto")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(length = 35)
	@NotEmpty(message = "wprowadź państwo")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	
	

	
}
