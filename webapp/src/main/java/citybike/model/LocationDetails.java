package citybike.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "location_details")
public class LocationDetails {
	
	private Long id;
	private User username;
	private String name;
	private Double xCoordinate;
	private Double yCoordinate;
	private String description;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	@OneToOne
	@JoinColumn(name = "username")
	public User getUsername() {
		return username;
	}
	public void setUsername(User username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getxCoordinate() {
		return xCoordinate;
	}
	public void setxCoordinate(Double xCoordinate) {
		this.xCoordinate = xCoordinate;
	}
	public Double getyCoordinate() {
		return yCoordinate;
	}
	public void setyCoordinate(Double yCoordinate) {
		this.yCoordinate = yCoordinate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
