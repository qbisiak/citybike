package citybike.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.money.Money;
import org.joda.time.LocalDateTime;

@Entity
@Table(name = "payments")
public class Payment {

	private Long id;
	private User user;
	//private Money amount;
	private BigDecimal amount;
	private LocalDateTime date;
	
	
	
	public Payment() {
		super();
	}


	public Payment(User user, BigDecimal amount) {
		super();
		this.user = user;
		this.amount = amount;
		this.date = new LocalDateTime();
	}
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	
	@Column(nullable = false)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	/*
	@Column(nullable = false)
	@Type(type="org.jadira.usertype.moneyandcurrency.joda.PersistentMoneyAmount")
	public Money getAmmount() {
		return amount;
	}
	public void setAmmount(Money amount) {
		this.amount = amount;
	}
	*/
	@Column(name = "transfered_on", nullable = false)
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	
}
