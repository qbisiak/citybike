package citybike.model;

import java.util.Set;

public class Roles {
		
	public static final Role ADMIN = new Role("ROLE_ADMIN", "Administrator");
	public static final Role ACCOUNTANT = new Role("ROLE_ACCOUNTANT", "Księgowy");
	public static final Role EMPLOYEE = new Role("ROLE_EMPLOYEE", "Pracownik");
	public static final Role CLIENT = new Role("ROLE_CLIENT", "Klient");
	public static final Role REMOTE = new Role("ROLE_REMOTE", "Stacja dokująca");
	
	public static boolean isEmployee(Role role)
	{
		return role.equals(ACCOUNTANT)
				|| role.equals(EMPLOYEE)
				|| role.equals(REMOTE)
				|| role.equals(ADMIN);
	}
	
	public static boolean isEmployee(Set<Role> set)
	{
		return set.contains(ACCOUNTANT)
				|| set.contains(EMPLOYEE)
				|| set.contains(REMOTE)
				|| set.contains(ADMIN);
	}
	
	public static boolean isClient(Role role)
	{
		return role.equals(CLIENT);
	}
	
	public static boolean isClient(Set<Role> set)
	{
		return set.contains(CLIENT);
	}
}
