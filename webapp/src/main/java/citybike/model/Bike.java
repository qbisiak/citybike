package citybike.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import citybike.client.model.ClientBike;

@Entity
@Table(name = "bikes")
public class Bike {

	private Long id;
	private String name;
	private boolean enabled;
	private boolean blocked;
	private String lockCode;
	private String description;
	private User user;
	private Set<BikeTransfer> loans = new HashSet<BikeTransfer>();
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length = 50)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}
	@Column(name = "lock_code")
	public String getLockCode() {
		return lockCode;
	}
	public void setLockCode(String lockCode) {
		this.lockCode = lockCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "username")
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	@OneToMany(fetch = FetchType.LAZY, mappedBy= "bike")
	public Set<BikeTransfer> getLoans() {
		return loans;
	}
	public void setLoans(Set<BikeTransfer> loans) {
		this.loans = loans;
	}
	
	public ClientBike toClientBike()
	{
		return new ClientBike(this.id, this.name, this.blocked, this.lockCode);
	}
	
}
