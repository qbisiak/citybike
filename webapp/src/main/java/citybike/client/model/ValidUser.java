package citybike.client.model;

import java.util.HashSet;
import java.util.Set;

public class ValidUser {
	
	private boolean valid;
	private boolean enabled;
	private boolean employee;
	private int borrowAbility;
	private String username;
	private Set<ClientBike> bikes = new HashSet<ClientBike>();
	
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isEmployee() {
		return employee;
	}
	public void setEmployee(boolean employee) {
		this.employee = employee;
	}
	public int getBorrowAbility() {
		return borrowAbility;
	}
	public void setBorrowAbility(int borrowAbility) {
		this.borrowAbility = borrowAbility;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Set<ClientBike> getBikes() {
		return bikes;
	}
	public void setBikes(Set<ClientBike> bikes) {
		this.bikes = bikes;
	}
	
	

}
