package citybike.client.model;

import java.math.BigDecimal;

public class Status {
	
	private boolean ok;
	
	
	
	public Status(boolean ok) {
		super();
		this.ok = ok;
	}
	public boolean isOk() {
		return ok;
	}
	public void setOk(boolean ok) {
		this.ok = ok;
	}

	
}
