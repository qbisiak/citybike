package citybike.comparator;

import java.util.Comparator;

import org.joda.time.LocalDateTime;

import citybike.model.BikeTransfer;

public class BikeTransferByDateComparator implements Comparator<BikeTransfer> {

	@Override
	public int compare(BikeTransfer transfer1, BikeTransfer transfer2) {
		
		LocalDateTime date1 = transfer1.getTimestamp();
		LocalDateTime date2 = transfer2.getTimestamp();
		
		
		if (date1.isEqual(date2))
			return 0;
		else if (date1.isAfter(date2))
			return 1;
		else
			return -1;
		
	}

	
	
}
