package citybike.view;

public class LocationView {
	
	private String id;
	private String name;
	private String description;
	private String url;
	private Integer available;
	private Integer lockedBikes;
	private Double x;
	private Double y;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getAvailable() {
		return available;
	}
	public void setAvailable(Integer available) {
		this.available = available;
	}
	
	
	public Integer getLockedBikes() {
		return lockedBikes;
	}
	public void setLockedBikes(Integer lockedBikes) {
		this.lockedBikes = lockedBikes;
	}
	public Double getX() {
		return x;
	}
	public void setX(Double x) {
		this.x = x;
	}
	public Double getY() {
		return y;
	}
	public void setY(Double y) {
		this.y = y;
	}


}
