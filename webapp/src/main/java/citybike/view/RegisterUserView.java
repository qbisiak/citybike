package citybike.view;

import javax.validation.Valid;
import javax.validation.constraints.*;

import citybike.model.UserDetails;

public class RegisterUserView {
	
	private String username;
	private String password;
	private String repeatedPassword;
	private UserDetails details;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Size(min = 2, max=30, message = "haslo musi zawierać od {min} do {max} znaków")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getRepeatedPassword() {
		return repeatedPassword;
	}
	public void setRepeatedPassword(String repeatedPassword) {
		this.repeatedPassword = repeatedPassword;
	}
	@Valid
	public UserDetails getDetails() {
		return details;
	}
	public void setDetails(UserDetails details) {
		this.details = details;
	}
	
	@AssertTrue(message="hasła muszą być takie same")
	public boolean getPasswordEquals()
	{
		if (this.password== null || this.repeatedPassword ==null)
			return false;
		else
			return this.password.equals(this.repeatedPassword);
	}
}
