package citybike.view;


public class TransactionUserView {
	
	private String id;
	private String kwota;
	private String kodBezpieczenstwa;
	private String md5sum;
	private String email;
	private String nazwisko;
	
	public String getMd5sum() {
		return md5sum;
	}
	public void setMd5sum(String md5sum) {
		this.md5sum = md5sum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNazwisko() {
		return nazwisko;
	}
	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getKwota() {
		return kwota;
	}
	public void setKwota(String kwota) {
		this.kwota = kwota;
	}
	public String getKodBezpieczenstwa() {
		return kodBezpieczenstwa;
	}
	public void setKodBezpieczenstwa(String kodBezpieczenstwa) {
		this.kodBezpieczenstwa = kodBezpieczenstwa;
	}
	
	
	
}
