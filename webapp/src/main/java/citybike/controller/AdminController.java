package citybike.controller;

import static citybike.navigator.Navigator.*;

import java.math.BigDecimal;
import java.security.Principal;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import citybike.model.Role;
import citybike.model.Roles;
import citybike.model.User;
import citybike.service.LocationService;
import citybike.service.SimulationService;
import citybike.service.UserService;
import citybike.view.NewUserView;

@Controller
@RequestMapping(value = ADMIN)
public class AdminController extends BaseController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	SimulationService simulationService;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String generateDate(){
		simulationService.clearAndGenerateBikeTransfers();
		return HOME;
	}
	
	@RequestMapping(value = {HOME, ROOT}, method = RequestMethod.GET)
	public String showEmployees(final ModelMap modelMap){
		
		modelMap.addAttribute("users", userService.getEmployees());
		modelMap.addAttribute("title", "Pracownicy");
		modelMap.addAttribute("NEW_USER_URL", ADMIN + NEW_USER);
		return ADMIN + USERS;
	}
	
	@RequestMapping(value = LOCATIONS, method = RequestMethod.GET)
	public String showLocations(final ModelMap modelMap){
		
		modelMap.addAttribute("users", userService.findByRole(Roles.REMOTE.getRole()));
		modelMap.addAttribute("title", "Lokalizacje");
		modelMap.addAttribute("NEW_USER_URL", ADMIN + NEW_USER);
		return ADMIN + USERS;
	}
	
	@RequestMapping(value = CLIENTS, method = RequestMethod.GET)
	public String showClients(final ModelMap modelMap){
		
		modelMap.addAttribute("users", userService.findByRole(Roles.CLIENT.getRole()));
		modelMap.addAttribute("title", "Klienci");
		modelMap.addAttribute("NEW_USER_URL", ADMIN + NEW_USER);
		return ADMIN + USERS;
	}
	
	@RequestMapping(value = NEW_USER, method = RequestMethod.GET)
	public String newUser(final Principal principal, final ModelMap modelMap){
		modelMap.addAttribute("user", new NewUserView());
		modelMap.addAttribute("roles", userService.getAllRoles());
		modelMap.addAttribute("ADD_USER_URL", ADMIN + ADD_USER);
		return ADMIN + NEW_USER;
	}
	
	@RequestMapping(value = ADD_USER, method = RequestMethod.POST)
	public String addUser(@ModelAttribute NewUserView user, final Principal principal, final ModelMap modelMap, RedirectAttributes redirectAttributes){
		userService.add(new User(user));
		redirectAttributes.addFlashAttribute("successMsg", "Użytkownik dodany pomyślnie.");
		return redirect(ADMIN + USERS, modelMap);
	}
}
