package citybike.controller;

import static citybike.navigator.Navigator.ACCOUNT;
import static citybike.navigator.Navigator.RECHARGE_ACCOUNT;
import static citybike.navigator.Navigator.SUCCESS_RECHARGE;
import static citybike.navigator.Navigator.ERROR_RECHARGE;
import static citybike.navigator.Navigator.WYN_RECHARGE;
import static citybike.navigator.Navigator.HOME;
import static citybike.navigator.Navigator.URL_LINK_BUILT;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.mapping.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import citybike.model.User;
import citybike.service.FinancialService;
import citybike.service.UserService;
import citybike.view.TransactionUserView;

@Controller
public class UserController extends BaseController{

	@Autowired
	UserService userService;
	
	@Autowired
	FinancialService financialService;

	@RequestMapping(value = {RECHARGE_ACCOUNT}, method = RequestMethod.GET)
	public String RechargeMyAccount(final Principal principal, final ModelMap modelMap){
		User u = userService.getWithDetails(principal.getName());
		TransactionUserView tr = new TransactionUserView();
		String id = "1010";
		String kwota = "10.00";
		String kodBezpieczenstwa = "demo";
		String md5sum = DigestUtils.md5Hex(id + kwota + kodBezpieczenstwa);
		if (u.getDetails() == null)
		{
			tr.setEmail(" ");
			tr.setNazwisko(" ");
		}
		else
		{
			tr.setEmail(u.getDetails().getEmail());
			tr.setNazwisko(u.getDetails().getLastName());
		}
		tr.setId(id);
		tr.setKodBezpieczenstwa(kodBezpieczenstwa);
		tr.setKwota(kwota);
		tr.setMd5sum(md5sum);
		
		modelMap.addAttribute("transaction", tr);
		return RECHARGE_ACCOUNT;
	}

	
	@RequestMapping(value = {ACCOUNT}, method = RequestMethod.GET)
	public String MyAccount(final Principal principal, final ModelMap modelMap){
		User u = userService.getWithDetails(principal.getName());
		modelMap.addAttribute("profile", u);
	
		return ACCOUNT;
	}
	
	@RequestMapping(value = {SUCCESS_RECHARGE}, method = RequestMethod.GET)
	public String RechargeSucces(final Principal principal, final ModelMap modelMap, RedirectAttributes redirectAttributes){
		BigDecimal money = new BigDecimal(10.00);
		User u = userService.getWithDetails(principal.getName());
		financialService.sendMoney(u.getUsername(), money);
		
		redirectAttributes.addFlashAttribute("successMsg", "Konto doładowane pomyślnie.");
		return redirect(ACCOUNT, modelMap);
	}
	
	@RequestMapping(value = {ERROR_RECHARGE}, method = RequestMethod.GET)
	public String RechargeError(final ModelMap modelMap, RedirectAttributes redirectAttributes){
		
		redirectAttributes.addFlashAttribute("failMsg", "Nie udało się doładować konta.");
		return redirect(ACCOUNT, modelMap);
	}
	
	@RequestMapping(value = WYN_RECHARGE , method = RequestMethod.POST)
	@ResponseBody
	public Boolean transactionConfirm(@RequestParam Map params) {
		return Boolean.TRUE;
	}
}

