package citybike.controller;

import static citybike.navigator.Navigator.ERROR;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController extends BaseController{

	@RequestMapping(value = ERROR + "/{code}")
    public String error(HttpServletRequest request, ModelMap model) {
        model.addAttribute("errorCode", request.getAttribute("javax.servlet.error.status_code"));
        Throwable throwable = (Throwable) request.getAttribute("javax.servlet.error.exception");
        String errorMessage = null;
        if (throwable != null) {
            errorMessage = throwable.getMessage();
            model.addAttribute("errorMessage", errorMessage.toString());
        }
        return ERROR;
    }
	
}
