package citybike.controller;

import static citybike.navigator.Navigator.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import citybike.service.AnalysisService;
import citybike.service.BikeService;
import citybike.service.LocationService;
import citybike.service.SimulationService;
import citybike.service.UserService;
import citybike.view.RegisterUserView;
import citybike.view.LocationView;

@Controller
public class HomeController extends BaseController {

	@Autowired
	LocationService locationService;

	@Autowired
	UserService userService;

	@Autowired
	BikeService bikeService;

	@Autowired
	SimulationService simulationService;

	@Autowired
	AnalysisService analysisService;

	List<LocationView> locations = new ArrayList<LocationView>();

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test(ModelMap model) {

		model.addAttribute(
				"infoMsg",
				"Optymalna liczba rowerów: "
						+ new Integer(analysisService.optimalNumberOfBikes())
								.toString());
		return HOME;
	}

	@RequestMapping(value = { HOME, ROOT, SUCCES_LOGIN }, method = RequestMethod.GET)
	public String showProfile(final Principal principal, final ModelMap modelMap) {
		// modelMap.addAttribute("successMsg", "Sukces!");
		// modelMap.addAttribute("infoMsg", "Strona w trakcie budowy!");
		// modelMap.addAttribute("warningMsg", "Uwaga!");
		// modelMap.addAttribute("failMsg", "Blad!");
		return HOME;
	}

	@RequestMapping(value = REGISTER, method = RequestMethod.GET)
	public String register(final ModelMap modelMap) {
		modelMap.addAttribute("user", new RegisterUserView());
		return REGISTER;
	}

	@RequestMapping(value = REGISTER, method = RequestMethod.POST)
	public String registration(
			@Valid @ModelAttribute("user") RegisterUserView user,
			BindingResult result, ModelMap modelMap,
			RedirectAttributes redirectAttributes) {

		String username = user.getUsername();
		if (username != null && userService.findByName(username) != null) {
			result.addError(new FieldError("user", "username",
					"login zajęty - wprowadź inny"));
		}

		if (result.hasErrors() == false) {

			userService.register(user);
			redirectAttributes.addFlashAttribute("successMsg",
					"Rejestracja pomyślna");
			redirectAttributes.addFlashAttribute("infoMsg",
					"Doładuj konto aby móc wypożyczyć rower");
			return redirect(RECHARGE_ACCOUNT, modelMap);
		}

		for (FieldError error : result.getFieldErrors()) {
			System.out.println(error.getField() + " "
					+ error.getDefaultMessage());
		}

		modelMap.addAttribute("warningMsg", "Sprawdź dane!");
		return REGISTER;
	}

	@RequestMapping(value = LOGIN, method = RequestMethod.GET)
	public String login(final Principal principal, final ModelMap modelMap) {
		return LOGIN;
	}

	@RequestMapping(value = LOGIN_ERROR, method = RequestMethod.GET)
	public String loginError(final Principal principal, final ModelMap modelMap) {
		modelMap.addAttribute("loginError", true);
		return LOGIN;
	}

	@RequestMapping(value = MAP, method = RequestMethod.GET)
	public String showMap(final Principal principal, final ModelMap modelMap) {

		locations = locationService.getAvailable();
		modelMap.addAttribute("locations", locations);
		return MAP;
	}

	@RequestMapping(value = MAP + ABOVE + "/{val}", method = RequestMethod.GET)
	public String showExcessBikesMap(@PathVariable Integer val,
			final ModelMap modelMap) {

		locations = locationService.getWithMoreThan(val);
		modelMap.addAttribute("locations", locations);
		return MAP;
	}

	@RequestMapping(value = MAP + BELOW + "/{val}", method = RequestMethod.GET)
	public String showFewBikesMap(@PathVariable Integer val,
			final ModelMap modelMap) {

		locations = locationService.getWithLessThan(val);
		modelMap.addAttribute("locations", locations);
		return MAP;
	}

	@RequestMapping(value = MAP + LOCKED, method = RequestMethod.GET)
	public String showLockedBikesMap(final ModelMap modelMap) {

		locations = locationService.getLocked();
		modelMap.addAttribute("locations", locations);
		return MAP;
	}

	@RequestMapping(value = DOWNLOAD_LOCATIONS, method = RequestMethod.POST, 
			produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<LocationView> downloadLocations() {
		return locations;
	}

	@RequestMapping(value = OFFER, method = RequestMethod.GET)
	public String showOffer(final Principal principal, final ModelMap modelMap) {

		return OFFER;
	}
}
