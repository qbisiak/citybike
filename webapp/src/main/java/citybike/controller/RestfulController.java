package citybike.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import citybike.client.model.ClientBike;
import citybike.client.model.Status;
import citybike.client.model.ValidUser;
import citybike.model.Bike;
import citybike.model.ExampleClass;
import citybike.service.BikeService;
import citybike.service.UserService;

import com.google.common.collect.Lists;

import static citybike.navigator.Navigator.*;

@RestController
@RequestMapping(value = REST)
public class RestfulController {
	
	@Autowired
	BikeService bikeService;
	
	@Autowired
	UserService userService;

	@RequestMapping(value = "/print", method = RequestMethod.GET)
	public ExampleClass printMessage() {
		ExampleClass obj = new ExampleClass();
		obj.setAge(20);
		obj.setCity("Łódź");
		obj.setList(Lists.newArrayList("element1", "element2", "element3"));
		obj.setName("Andrzej");
		obj.setSurname("Piaseczny");
		return obj;
 
	}
	
	@RequestMapping(value = MY_BIKES, method =  RequestMethod.GET)
	public Set<ClientBike> getMyBikes(final Principal principal){
		
		Set<ClientBike> bikes = new HashSet<ClientBike>();
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		for (Bike bike : bikeService.findByUsername(username)) {
			bikes.add(bike.toClientBike());
		}
		
		return bikes;
	}
	
	@RequestMapping(value = USER_VALIDATION, method = {RequestMethod.GET, RequestMethod.POST})
	public ValidUser userValidation(@RequestParam String username, @RequestParam String password){
		
		return userService.getValidUser(username, password);
	}
	
	@RequestMapping(value = RENT, method = {RequestMethod.GET, RequestMethod.POST})
	public Status rentBike(@RequestParam String username, @RequestParam Long bike){
		
		try	{
			
			bikeService.transfer(bike, username);
			return new Status(true);
			
		} catch (Exception ex) {
			
			return new Status(false);
		}

	}
	
	@RequestMapping(value = RETURN, method = {RequestMethod.GET, RequestMethod.POST})
	public Status returnBike(@RequestParam Long bike){
		
		try	{
			
			String location = SecurityContextHolder.getContext().getAuthentication().getName();
			bikeService.transfer(bike, location);
			return new Status(true);
			
		} catch (Exception ex) {
			
			return new Status(false);
		}
	}
}
