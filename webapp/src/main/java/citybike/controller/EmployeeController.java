package citybike.controller;

import static citybike.navigator.Navigator.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import citybike.model.Bike;
import citybike.model.Roles;
import citybike.model.User;
import citybike.service.AnalysisService;
import citybike.service.BikeService;
import citybike.service.UserService;

@Controller
@RequestMapping(value = EMPLOYEE)
public class EmployeeController extends BaseController{
	
	@Autowired
	UserService userService;
	
	@Autowired
	BikeService bikeService;
	
	@Autowired
	AnalysisService analysisService;
	
	List<User> users = new ArrayList<User>();

	@RequestMapping(value = {HOME, ROOT, BIKES}, method = RequestMethod.GET)
	public String showEmployeesBikes(final Principal principal, final ModelMap modelMap){
		users = userService.getEmployeesWithBikes();
		List<Bike> bikesLoanedByUsers = new ArrayList<Bike>();
		for (User user : users) {
			bikesLoanedByUsers.addAll(user.getBikes());
		}
		modelMap.addAttribute("users", users);
		
		return EMPLOYEE + BIKES;
	}
	
	@RequestMapping(value = {LOCATION_BIKES}, method = RequestMethod.GET)
	public String showLocalizationBikes(final Principal principal, final ModelMap modelMap){
		users = userService.findByRoleWithBikes(Roles.REMOTE.getRole());
		List<Bike> bikesLoanedByLocalizations = new ArrayList<Bike>();
		for (User user : users) {
			bikesLoanedByLocalizations.addAll(user.getBikes());
		}
		modelMap.addAttribute("users", users);
		
		return EMPLOYEE + BIKES;
	}
	
	@RequestMapping(value = {CLIENT_BIKES}, method = RequestMethod.GET)
	public String showClientBikes(final Principal principal, final ModelMap modelMap){
		users = userService.findByRoleWithBikes(Roles.CLIENT.getRole());
		List<Bike> bikesLoanedByLocalizations = new ArrayList<Bike>();
		for (User user : users) {
			bikesLoanedByLocalizations.addAll(user.getBikes());
		}
		modelMap.addAttribute("users", users);
		
		return EMPLOYEE + BIKES;
	}
	
	
	@RequestMapping(value = {BIKE}, method = RequestMethod.GET)
	public String showBike(@RequestParam("id") String name, final Principal principal, final ModelMap modelMap){

		modelMap.addAttribute("bike", bikeService.getWithBikeTransfers(name));
		
		return EMPLOYEE + BIKE;
	}
	
	@RequestMapping(value = {BIKE_BLOCK}, method = RequestMethod.GET)
	public String blockBike(@RequestParam("id") String name, final Principal principal, final ModelMap modelMap){
		Bike bike = bikeService.getWithBikeTransfers(name);
		bike.setBlocked(!bike.isBlocked());
		bikeService.update(bike);
		modelMap.addAttribute("bike", bike);
		return EMPLOYEE + BIKE;
	}
	
	@RequestMapping(value = PROFILE + "/{username}", method = RequestMethod.GET)
	public String showProfile(@PathVariable String username, Principal principal, ModelMap modelMap) {
		User u = userService.getWithDetails(username);
		modelMap.addAttribute("profile", u);
	
		return ACCOUNT;
	}
	
	@RequestMapping(value = PROFILE + "/{username}" + "/lock", method = RequestMethod.GET)
	public String changeLockStatus(@PathVariable String username, ModelMap modelMap) {
		userService.changeLockStatus(username);
		
		return redirect(EMPLOYEE + PROFILE + "/" + username, modelMap);
	}
	
	@RequestMapping(value = STATS, method = RequestMethod.GET)
	public String showBikesStats(ModelMap modelMap) {
		int allBikes = bikeService.countEnabled();
		int lockedBikes = bikeService.countLocked();
		int unlockedBikes = allBikes - lockedBikes;
		int optimalBikes = analysisService.optimalNumberOfBikes();
		
		modelMap.addAttribute("allBikes", allBikes);
		modelMap.addAttribute("lockedBikes", lockedBikes);
		modelMap.addAttribute("unlockedBikes", unlockedBikes);
		
		modelMap.addAttribute("optimalBikes", optimalBikes);
		modelMap.addAttribute("bikesDiff", optimalBikes - unlockedBikes);
		
		return EMPLOYEE + STATS;
	}
}

