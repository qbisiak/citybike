package citybike.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import citybike.model.User;
import citybike.service.UserService;

@Controller
public class BaseController {

	@Autowired
	UserService userService;
	
	@ModelAttribute("currentUser")
	public User currentUser(Principal principal){
		if (principal != null)
			return userService.findByName(principal.getName());
		else 
			return null;
	}
	
	
	public String redirect(String url, ModelMap model){
		model.remove("currentUser");
		return "redirect:" + url;
	}
}
