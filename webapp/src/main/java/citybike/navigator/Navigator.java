package citybike.navigator;

public class Navigator {

	public static final String ROOT = "/";
	public static final String HOME = "/home";
	public static final String LOGIN = "/login";
	public static final String LOGIN_ERROR = "/error-login";
	public static final String SUCCES_LOGIN = "/success-login";
	public static final String ERROR = "/error";
	
	public static final String ADMIN = "/admin";
	public static final String USERS = "/home";
	public static final String ADD_USER = "/add-user";
	public static final String NEW_USER = "/new-user";
	public static final String CLIENTS = "/clients";
	public static final String LOCATIONS = "/locations";
			
	
	public static final String REST = "/restful";
	public static final String MY_BIKES = "/my-bikes";
	public static final String USER_VALIDATION = "/user-validation";
	public static final String RENT = "/rent";
	public static final String RETURN = "/return";
	
	public static final String MAP ="/public/map";
	public static final String ABOVE = "/above";
	public static final String BELOW = "/below";
	public static final String LOCKED = "/locked";
	public static final String DOWNLOAD_LOCATIONS ="/public/map/downloadLocations";
	public static final String OFFER = "/public/offer";
	
	public static final String REGISTER = "/public/register";
	
	public static final String EMPLOYEE = "/employee";
	public static final String BIKES = "/bikes";
	public static final String LOCATION_BIKES = "/bikes/locations";
	public static final String CLIENT_BIKES = "/bikes/clients";
	public static final String BIKE = "/bike";
	public static final String BIKE_BLOCK = "/bike/block";
	public static final String PROFILE = "/profile";
	public static final String STATS = "/stats";
	
	public static final String RECHARGE_ACCOUNT = "/user/recharge_account";
	public static final String ACCOUNT = "/user/account";
	public static final String SUCCESS_RECHARGE = "/user/success-recharge";
	public static final String ERROR_RECHARGE = "/user/error-recharge";
	public static final String WYN_RECHARGE = "/user/wyn-recharge";
	public static final String URL_LINK_BUILT = "urlLinkBuilt";
}
