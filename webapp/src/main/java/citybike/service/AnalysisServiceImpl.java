package citybike.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.comparator.BikeTransferByDateComparator;
import citybike.model.BikeTransfer;
import citybike.model.Roles;
import citybike.model.User;

@Service
@Transactional
public class AnalysisServiceImpl implements AnalysisService {
	
	public final static int MIN_BIKES = 2;
	public final static int WEEKS = 2;
	
	@Autowired
	BikeService bikeService;
	
	@Autowired
	UserService userService;
	
	public int optimalNumberOfBikes()
	{
		
		List<User> locations = userService.findByRole(Roles.REMOTE.getRole());		
		int bikesNeeded = bikeService.countEnabled();
		
		for (User location : locations) {
			
			bikesNeeded -= bikesNeeded(location);
			
		}
		
		return bikesNeeded;
	}
	
	private int bikesNeeded(User location)
	{
		
		List<BikeTransfer> transfers = new ArrayList<BikeTransfer>(location.getReturns().size() + location.getLoans().size());
		transfers.addAll(location.getReturns());
		transfers.addAll(location.getLoans());

		Collections.sort(transfers, new BikeTransferByDateComparator());
		
		int bikesCount = location.getBikes().size();
		int min = bikesCount;
		int max = bikesCount;
		
		BikeTransfer currentTransfer;
		
		LocalDateTime startDate = new LocalDateTime().minusWeeks(WEEKS);
		boolean oldDate = false;
		
		//odtwarzanie od tyłu ilośći rowerów w stacji dokującej.
		for (int i= transfers.size() - 1; !oldDate && i >= 0; i--)
		{
			currentTransfer = transfers.get(i);
			oldDate = currentTransfer.getTimestamp().isBefore(startDate);
			
			if (oldDate == false)
			{
				if (currentTransfer.getToUser().equals(location))
					bikesCount--;
				else
					bikesCount++;
				
				if (bikesCount > max) max = bikesCount;
				if (bikesCount < min) min = bikesCount;
			}
		}
		
		return MIN_BIKES - min;
	}
	
}
