package citybike.service;

import java.util.List;


import citybike.view.LocationView;

public interface LocationService {
//	public void add(Location location);
//	public List<Location> getAll();
	public List<LocationView> getAvailable();
	public List<LocationView> getLocked();
	public List<LocationView> getWithMoreThan(final int above);
	public List<LocationView> getWithLessThan(final int below);
}
