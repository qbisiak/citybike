package citybike.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.dao.BikeDao;
import citybike.dao.UserDao;
import citybike.model.Bike;
import citybike.model.BikeTransfer;
import citybike.model.Role;
import citybike.model.User;

@Service
@Transactional
public class BikeServiceImpl implements BikeService {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	BikeDao bikeDao;
	
	@Autowired
	FinancialService financialService;

	@Override
	public Set<Bike> getAvailable(String locationId) {
		User location = userDao.findByUserName(locationId);
		return location.getBikes();
	}
	
	

	@Override
	public Integer countEnabled() {
		return bikeDao.countEnabled();
	}



	@Override
	public Integer countLocked() {
		return bikeDao.countLocked();
	}



	@Override
	public void transfer(Long bikeId, String toUsername) {
		Bike bike = bikeDao.findById(bikeId);
		User currentUser = bike.getUser();
		User destUser = userDao.findByUserName(toUsername);
		bike.setUser(destUser);

		
		BikeTransfer transfer = new BikeTransfer();
		transfer.setFromUser(currentUser);
		transfer.setToUser(destUser);
		transfer.setBike(bike);
		transfer.setTimestamp(new LocalDateTime());
		
		if (currentUser.getRoles().contains(new Role("ROLE_CLIENT", null)))
		{
			BigDecimal fare = financialService.calculateFare(transfer);
			financialService.charge(currentUser.getUsername(), fare);
			transfer.setFare(fare);
		}
		else
			transfer.setFare(new BigDecimal("0.00"));  
		
		bikeDao.update(bike);
		bikeDao.saveTransfer(transfer);

		
	}

	@Override
	public Bike get(String name) {
		return bikeDao.findByName(name);
	}
	
	@Override
	public Bike getWithBikeTransfers(String name) {
		Bike bike = bikeDao.findByName(name);
		Hibernate.initialize(bike.getLoans());
		return bike;
	}

	@Override
	public Set<Bike> findByUsername(String username) {
		User user = userDao.findByUserName(username);
		Hibernate.initialize(user.getBikes());
		return user.getBikes();
	}

	@Override
	public Set<Bike> getLoaned() {
		
		List<User> clients = userDao.findUsersByRole("ROLE_CLIENT");
		Set<Bike> loanedBikes = new HashSet<Bike>();
		
		for (User client : clients) {
			loanedBikes.addAll(client.getBikes());
		}
		
		return loanedBikes;
	}

	@Override
	public BikeTransfer lastTransfer(Bike bike, User toUser) {
		return bikeDao.lastTransfer(bike.getId(), toUser.getUsername());
	}



	@Override
	public void update(Bike bike) {
			bikeDao.update(bike);
	}
	
	
	
}
