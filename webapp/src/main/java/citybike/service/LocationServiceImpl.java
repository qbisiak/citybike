package citybike.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.dao.LocationDao;
import citybike.dao.UserDao;
import citybike.model.Bike;
import citybike.model.LocationDetails;
import citybike.model.User;
import citybike.view.LocationView;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {

	@Autowired
	UserDao userDao;

//	public void add(Location location) {
//		locationDao.add(location);
//	}
//
//	@Override
//	public List<Location> getAll() {
//		return locationDao.getAll();
//	}
//
	@Override
	public List<LocationView> getAvailable() {
		List<User> locations =  userDao.findUsersByRole("ROLE_REMOTE");
		List<LocationView> locationViews = new ArrayList<LocationView>(locations.size());
		LocationView lv;
		LocationDetails details;
		Set<Bike> bikes;
		
		for (User location : locations) {
			lv = new LocationView();
			lv.setId(location.getUsername());
			lv.setUrl("");
			details = location.getLocationDetails();
			
			if (details != null)
			{
				lv.setName(details.getName());
				lv.setDescription(details.getDescription());
				lv.setX(details.getxCoordinate());
				lv.setY(details.getyCoordinate());
			}
			
			bikes = location.getBikes();

			if (bikes != null)
			{
				Integer availableBikes = bikes.size();
				Integer lockedBikes = 0;
				
				for (Bike bike : bikes) {
					if (bike.isBlocked())
					{
						availableBikes--;
						lockedBikes++;
					}
				}
				lv.setAvailable(availableBikes);
				lv.setLockedBikes(lockedBikes);
				
			}
			
			locationViews.add(lv);
		}
		
		return locationViews;
	}

	@Override
	public List<LocationView> getLocked() {
		List<LocationView> locations = getAvailable();
		LocationView location;
		Iterator<LocationView> it = locations.iterator();
		while (it.hasNext()) {
			location = it.next();
			if (location.getLockedBikes() == 0)
				it.remove();
		}
		return locations;
	}

	@Override
	public List<LocationView> getWithMoreThan(int above) {
		List<LocationView> locations = getAvailable();
		LocationView location;
		Iterator<LocationView> it = locations.iterator();
		while (it.hasNext()) {
			location = it.next();
			if (location.getAvailable() < above)
				it.remove();
		}
		return locations;
	}

	@Override
	public List<LocationView> getWithLessThan(int below) {
		List<LocationView> locations = getAvailable();
		LocationView location;
		Iterator<LocationView> it = locations.iterator();
		while (it.hasNext()) {
			location = it.next();
			if (location.getAvailable() > below)
				it.remove();
		}
		return locations;
	}


}
