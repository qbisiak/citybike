package citybike.service;

import java.util.Set;

import citybike.model.Bike;
import citybike.model.BikeTransfer;
import citybike.model.User;

public interface BikeService {
	public Set<Bike> getAvailable(String locationId);
	public void transfer(Long bikeId, String toUsername);
	public Bike get(String bikeId);
	public Set<Bike> findByUsername(String username);
	public Set<Bike> getLoaned();
	public BikeTransfer lastTransfer(Bike bike, User toUser);
	public Integer countEnabled();
	public Integer countLocked();
	public Bike getWithBikeTransfers(String name);
	public void update(Bike bike);
}
