package citybike.service;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.client.model.ClientBike;
import citybike.client.model.ValidUser;
import citybike.dao.UserDao;
import citybike.exception.UserExistsException;
import citybike.model.Bike;
import citybike.model.Role;
import citybike.model.Roles;
import citybike.model.User;
import citybike.model.UserDetails;
import citybike.view.RegisterUserView;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	public static final int EMPLOYEE_MAX_BIKES = Integer.MAX_VALUE;
	public static final int CLIENT_MAX_BIKES = 4;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Override
	public void add(User user) {
		PasswordEncoder encoder = new Md5PasswordEncoder();
		user.setPassword(encoder.encodePassword(user.getPassword(), null));
		userDao.add(user);
	}

	@Override
	public void disable(String username) {
		userDao.disable(username);
		
	}
	
	

	@Override
	public List<User> findByRole(String role) {
		return userDao.findUsersByRole(role);
	}
	
	@Override
	public List<User> findByRoleWithBikes(String role) {
		List<User> users = userDao.findUsersByRole(role);
		for (User user : users) {
			Hibernate.initialize(user.getBikes());
		}
		return users;
	}

	@Override
	public List<User> getEmployees() {
		return userDao.findEmployees();
	}

	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Override
	public List<Role> getAllRoles() {
		return userDao.getAllRoles();
	}

	@Override
	public void register(RegisterUserView userView) {
		User user = new User(userView);
		user.getRoles().add(Roles.CLIENT);
		user.setPassword(passwordEncoder.encodePassword(user.getPassword(), null));
		userDao.add(user);
		UserDetails details = user.getDetails();
		details.setUser(user);
		//userDao.add(user.getDetails());
	}

	@Override
	public User findByName(String username) {
		return userDao.findByUserName(username);
	}
	
	

	@Override
	public User getWithDetails(String username) {
		User user = userDao.findByUserName(username);
		Hibernate.initialize(user.getDetails());
		Hibernate.initialize(user.getBikes());
		Hibernate.initialize(user.getReturns());
		Hibernate.initialize(user.getLoans());
		Hibernate.initialize(user.getPayments());
		return user;
	}

	@Override
	public void update(User user) {
		userDao.update(user);
		
	}

	@Override
	public ValidUser getValidUser(String username, String password) {
		
		ValidUser validUser = new ValidUser();
		validUser.setUsername(username);
		
		User user = userDao.findByUserName(username);
		boolean valid =
				user != null
				&& passwordEncoder.isPasswordValid(user.getPassword(), password, null)
				&& user.isEnabled();
		
		
		validUser.setValid(valid);
		
		if (valid)	{
			
			boolean isEmployee = Roles.isEmployee(user.getRoles());
			validUser.setEmployee(isEmployee);
			
			validUser.setEnabled(user.isBlocked() == false && (user.getBalance().compareTo(BigDecimal.ZERO) > 0 || isEmployee));
			
			Set<Bike> bikes = user.getBikes();
			Set<ClientBike> clientsBikes = new HashSet<ClientBike>();
			
			for (Bike bike : bikes) {
				clientsBikes.add(bike.toClientBike());
			}
			
			validUser.setBikes(clientsBikes);

			
			if (isEmployee)	{
				validUser.setBorrowAbility(EMPLOYEE_MAX_BIKES - bikes.size());
			}
			else {
				validUser.setBorrowAbility(CLIENT_MAX_BIKES - bikes.size());
			}
		
		}
		return validUser;
	}

	@Override
	public List<User> getEmployeesWithBikes() {
			List<User> users = userDao.findEmployees();
			for (User user : users) {
				Hibernate.initialize(user.getBikes());
			}
			return users;
	}

	@Override
	public void changeLockStatus(String username) {
		User user = userDao.findByUserName(username);
		user.setBlocked(!user.isBlocked());
		userDao.update(user);
		
	}


	
	
	
}
