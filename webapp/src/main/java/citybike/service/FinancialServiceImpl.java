package citybike.service;

import java.math.BigDecimal;

import org.joda.time.Minutes;
import org.joda.time.Period;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.dao.PaymentDao;
import citybike.model.BikeTransfer;
import citybike.model.Payment;
import citybike.model.User;

@Service
@Transactional
public class FinancialServiceImpl implements FinancialService {

	@Autowired
	UserService userService;
	
	@Autowired
	BikeService bikeService;
	
	@Autowired
	PaymentDao paymentDao;
	
	@Override
	public void sendMoney(String username, BigDecimal amount) {
		User user = userService.findByName(username);
		user.setBalance(user.getBalance().add(amount));
		userService.update(user);
		paymentDao.add(new Payment(user, amount));
	}

	@Override
	public void charge(String username, BigDecimal amount) {
		User user = userService.findByName(username);
		user.setBalance(user.getBalance().subtract(amount));
		userService.update(user);
		paymentDao.add(new Payment(user, BigDecimal.ZERO.subtract(amount)));
	}

	@Override
	public BigDecimal calculateFare(BikeTransfer bikeTransfer) {
		
		BikeTransfer prevBikeTransfer = bikeService.lastTransfer(bikeTransfer.getBike(), bikeTransfer.getFromUser());
		if (prevBikeTransfer!= null){
			
			Integer minutes = Minutes.minutesBetween(
					prevBikeTransfer.getTimestamp(),
					bikeTransfer.getTimestamp()
					).getMinutes();
			
			//TODO: normalne oblicanie cen;
			Double fare = 2.0;
			fare = fare * (double)minutes / 60.0;
			BigDecimal fareDecimal = new BigDecimal(fare);
	
			return fareDecimal;
		}
		else {
			return new BigDecimal("0.00");
		}
	}

}
