package citybike.service;

import java.math.BigDecimal;

import citybike.model.BikeTransfer;

public interface FinancialService {
	
	public void sendMoney(String username, BigDecimal amount);
	public void charge(String username, BigDecimal amount);
	public BigDecimal calculateFare(BikeTransfer bikeTransfer);
}
