package citybike.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import citybike.dao.BikeDao;
import citybike.dao.UserDao;
import citybike.model.Bike;
import citybike.model.BikeTransfer;
import citybike.model.Roles;
import citybike.model.User;

@Service
@Transactional
public class SimulationService {
	
	final static int WEEKS = 3;
	
	@Autowired
	BikeDao bikeDao;
	
	@Autowired
	BikeService bikeService;
	
	@Autowired
	UserDao userDao;

	public void clearAndGenerateBikeTransfers()
	{	
	
		
		List<User> clients = userDao.findUsersByRole(Roles.CLIENT.getRole());
		List<User> locations = userDao.findUsersByRole(Roles.REMOTE.getRole());
		List<Bike> bikes = bikeDao.getAll();
		
		int clientsCount = clients.size();
		int locationsCount = locations.size();
		int bikesCount = bikes.size();
		
		LocalDateTime nowDateTime = new LocalDateTime();
		LocalDateTime consideredDateTime = nowDateTime.minusWeeks(WEEKS);
		
		bikeDao.clearTransfers();
		
		Random rand = new Random();
		Bike randBike;
		BikeTransfer bikeTransfer;
		User newUser;
		
		while(consideredDateTime.isBefore(nowDateTime))
		{
			randBike = bikes.get(rand.nextInt(bikesCount));
			
			if (randBike.getUser().getRoles().contains(Roles.CLIENT))
			{
				newUser = locations.get(rand.nextInt(locationsCount));
			}
			else
			{
				newUser = clients.get(rand.nextInt(clientsCount));
			}
			
			bikeTransfer = new BikeTransfer(null, randBike, consideredDateTime, randBike.getUser(), newUser, BigDecimal.ZERO);
			bikeDao.saveTransfer(bikeTransfer);
			randBike.setUser(newUser);
			
			// plus 0s - 10 min
			consideredDateTime = consideredDateTime.plusSeconds(rand.nextInt(600));
		}
		
		
	}
	
}
