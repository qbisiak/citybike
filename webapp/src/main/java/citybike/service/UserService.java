package citybike.service;

import java.util.List;

import citybike.client.model.ValidUser;
import citybike.model.Role;
import citybike.model.User;
import citybike.view.RegisterUserView;


public interface UserService {
	public void add(User user);
	public void disable(String username);
	public List<User> getAll();
	public List<User> getEmployees();
	public List<User> getEmployeesWithBikes();
	public List<Role> getAllRoles();
	public void register(RegisterUserView user);
	public User findByName(String username);
	public User getWithDetails(String username);
	public List<User> findByRole(String role);
	public List<User> findByRoleWithBikes(String role);
	public void update(User user);
	public ValidUser getValidUser(String username, String password);
	public void changeLockStatus(String username);
}
