﻿namespace CityBike.View
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda wsparcia projektanta - nie należy modyfikować
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLoginError = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.tbPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tbLogin = new System.Windows.Forms.TextBox();
            this.btnReturnBike = new System.Windows.Forms.Button();
            this.btnRentBike = new System.Windows.Forms.Button();
            this.btnCancel2 = new System.Windows.Forms.Button();
            this.btnChoseBike = new System.Windows.Forms.Button();
            this.lbBikes = new System.Windows.Forms.ListBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.labelUser = new System.Windows.Forms.Label();
            this.labelLocationInfo = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.labelStatus = new System.Windows.Forms.Label();
            this.labelConfirm = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelBikesLeft = new System.Windows.Forms.Label();
            this.panelConnect = new System.Windows.Forms.Panel();
            this.panelLogin = new System.Windows.Forms.Panel();
            this.panelOperations = new System.Windows.Forms.Panel();
            this.panelBikesList = new System.Windows.Forms.Panel();
            this.panelConfirm = new System.Windows.Forms.Panel();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.panelConnect.SuspendLayout();
            this.panelLogin.SuspendLayout();
            this.panelOperations.SuspendLayout();
            this.panelBikesList.SuspendLayout();
            this.panelConfirm.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelLoginError
            // 
            this.labelLoginError.AutoSize = true;
            this.labelLoginError.Location = new System.Drawing.Point(3, 9);
            this.labelLoginError.Name = "labelLoginError";
            this.labelLoginError.Size = new System.Drawing.Size(30, 13);
            this.labelLoginError.TabIndex = 5;
            this.labelLoginError.Text = "Błąd";
            // 
            // btnLogin
            // 
            this.btnLogin.Location = new System.Drawing.Point(65, 114);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(100, 45);
            this.btnLogin.TabIndex = 4;
            this.btnLogin.Text = "Zaloguj się";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // tbPassword
            // 
            this.tbPassword.Location = new System.Drawing.Point(65, 78);
            this.tbPassword.Name = "tbPassword";
            this.tbPassword.PasswordChar = '*';
            this.tbPassword.Size = new System.Drawing.Size(100, 20);
            this.tbPassword.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(2, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hasło";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Login";
            // 
            // tbLogin
            // 
            this.tbLogin.Location = new System.Drawing.Point(65, 43);
            this.tbLogin.Name = "tbLogin";
            this.tbLogin.Size = new System.Drawing.Size(100, 20);
            this.tbLogin.TabIndex = 0;
            // 
            // btnReturnBike
            // 
            this.btnReturnBike.Location = new System.Drawing.Point(6, 120);
            this.btnReturnBike.Name = "btnReturnBike";
            this.btnReturnBike.Size = new System.Drawing.Size(191, 46);
            this.btnReturnBike.TabIndex = 1;
            this.btnReturnBike.Text = "Oddaj rower";
            this.btnReturnBike.UseVisualStyleBackColor = true;
            this.btnReturnBike.Click += new System.EventHandler(this.btnReturnBike_Click);
            // 
            // btnRentBike
            // 
            this.btnRentBike.Location = new System.Drawing.Point(6, 68);
            this.btnRentBike.Name = "btnRentBike";
            this.btnRentBike.Size = new System.Drawing.Size(191, 46);
            this.btnRentBike.TabIndex = 0;
            this.btnRentBike.Text = "Wypożycz";
            this.btnRentBike.UseVisualStyleBackColor = true;
            this.btnRentBike.Click += new System.EventHandler(this.rentBike_Click_1);
            // 
            // btnCancel2
            // 
            this.btnCancel2.Location = new System.Drawing.Point(112, 276);
            this.btnCancel2.Name = "btnCancel2";
            this.btnCancel2.Size = new System.Drawing.Size(85, 37);
            this.btnCancel2.TabIndex = 2;
            this.btnCancel2.Text = "Anuluj";
            this.btnCancel2.UseVisualStyleBackColor = true;
            this.btnCancel2.Click += new System.EventHandler(this.btnCancel2_Click_1);
            // 
            // btnChoseBike
            // 
            this.btnChoseBike.Location = new System.Drawing.Point(3, 276);
            this.btnChoseBike.Name = "btnChoseBike";
            this.btnChoseBike.Size = new System.Drawing.Size(85, 37);
            this.btnChoseBike.TabIndex = 1;
            this.btnChoseBike.Text = "Wybierz";
            this.btnChoseBike.UseVisualStyleBackColor = true;
            this.btnChoseBike.Click += new System.EventHandler(this.btnChoseBike_Click);
            // 
            // lbBikes
            // 
            this.lbBikes.FormattingEnabled = true;
            this.lbBikes.Location = new System.Drawing.Point(3, 6);
            this.lbBikes.Name = "lbBikes";
            this.lbBikes.Size = new System.Drawing.Size(194, 264);
            this.lbBikes.TabIndex = 0;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(2, 103);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(193, 58);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Anuluj";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(2, 39);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(193, 58);
            this.btnConfirm.TabIndex = 0;
            this.btnConfirm.Text = "Potwierdź";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Enabled = false;
            this.btnLogout.Location = new System.Drawing.Point(6, 195);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Size = new System.Drawing.Size(191, 46);
            this.btnLogout.TabIndex = 3;
            this.btnLogout.Text = "Wyloguj";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // labelUser
            // 
            this.labelUser.AutoSize = true;
            this.labelUser.Location = new System.Drawing.Point(3, 32);
            this.labelUser.Name = "labelUser";
            this.labelUser.Size = new System.Drawing.Size(29, 13);
            this.labelUser.TabIndex = 4;
            this.labelUser.Text = "login";
            // 
            // labelLocationInfo
            // 
            this.labelLocationInfo.AutoSize = true;
            this.labelLocationInfo.Location = new System.Drawing.Point(3, 6);
            this.labelLocationInfo.Name = "labelLocationInfo";
            this.labelLocationInfo.Size = new System.Drawing.Size(85, 13);
            this.labelLocationInfo.TabIndex = 5;
            this.labelLocationInfo.Text = "Brak połączenia";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(3, 22);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(194, 41);
            this.btnStart.TabIndex = 6;
            this.btnStart.Text = "Połącz";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(3, 6);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(35, 13);
            this.labelStatus.TabIndex = 5;
            this.labelStatus.Text = "status";
            // 
            // labelConfirm
            // 
            this.labelConfirm.AutoSize = true;
            this.labelConfirm.Location = new System.Drawing.Point(3, 6);
            this.labelConfirm.Name = "labelConfirm";
            this.labelConfirm.Size = new System.Drawing.Size(35, 13);
            this.labelConfirm.TabIndex = 2;
            this.labelConfirm.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 6;
            // 
            // labelBikesLeft
            // 
            this.labelBikesLeft.AutoSize = true;
            this.labelBikesLeft.Location = new System.Drawing.Point(3, 45);
            this.labelBikesLeft.Name = "labelBikesLeft";
            this.labelBikesLeft.Size = new System.Drawing.Size(116, 13);
            this.labelBikesLeft.TabIndex = 7;
            this.labelBikesLeft.Text = "Dostępnych rowerów : ";
            // 
            // panelConnect
            // 
            this.panelConnect.Controls.Add(this.labelLocationInfo);
            this.panelConnect.Controls.Add(this.btnStart);
            this.panelConnect.Location = new System.Drawing.Point(12, 27);
            this.panelConnect.Name = "panelConnect";
            this.panelConnect.Size = new System.Drawing.Size(200, 324);
            this.panelConnect.TabIndex = 7;
            // 
            // panelLogin
            // 
            this.panelLogin.Controls.Add(this.labelLoginError);
            this.panelLogin.Controls.Add(this.btnLogin);
            this.panelLogin.Controls.Add(this.tbLogin);
            this.panelLogin.Controls.Add(this.tbPassword);
            this.panelLogin.Controls.Add(this.label1);
            this.panelLogin.Controls.Add(this.label2);
            this.panelLogin.Location = new System.Drawing.Point(218, 27);
            this.panelLogin.Name = "panelLogin";
            this.panelLogin.Size = new System.Drawing.Size(200, 324);
            this.panelLogin.TabIndex = 8;
            this.panelLogin.Visible = false;
            // 
            // panelOperations
            // 
            this.panelOperations.Controls.Add(this.labelBikesLeft);
            this.panelOperations.Controls.Add(this.labelStatus);
            this.panelOperations.Controls.Add(this.label3);
            this.panelOperations.Controls.Add(this.btnLogout);
            this.panelOperations.Controls.Add(this.labelUser);
            this.panelOperations.Controls.Add(this.btnReturnBike);
            this.panelOperations.Controls.Add(this.btnRentBike);
            this.panelOperations.Location = new System.Drawing.Point(424, 27);
            this.panelOperations.Name = "panelOperations";
            this.panelOperations.Size = new System.Drawing.Size(200, 324);
            this.panelOperations.TabIndex = 9;
            this.panelOperations.Visible = false;
            // 
            // panelBikesList
            // 
            this.panelBikesList.Controls.Add(this.btnCancel2);
            this.panelBikesList.Controls.Add(this.lbBikes);
            this.panelBikesList.Controls.Add(this.btnChoseBike);
            this.panelBikesList.Location = new System.Drawing.Point(630, 27);
            this.panelBikesList.Name = "panelBikesList";
            this.panelBikesList.Size = new System.Drawing.Size(200, 324);
            this.panelBikesList.TabIndex = 10;
            this.panelBikesList.Visible = false;
            // 
            // panelConfirm
            // 
            this.panelConfirm.Controls.Add(this.labelConfirm);
            this.panelConfirm.Controls.Add(this.btnCancel);
            this.panelConfirm.Controls.Add(this.btnConfirm);
            this.panelConfirm.Location = new System.Drawing.Point(836, 27);
            this.panelConfirm.Name = "panelConfirm";
            this.panelConfirm.Size = new System.Drawing.Size(200, 324);
            this.panelConfirm.TabIndex = 11;
            this.panelConfirm.Visible = false;
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.settingsToolStripMenuItem.Text = "Ustawienia";
            this.settingsToolStripMenuItem.Click += new System.EventHandler(this.settingsToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1043, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1043, 357);
            this.Controls.Add(this.panelConfirm);
            this.Controls.Add(this.panelBikesList);
            this.Controls.Add(this.panelOperations);
            this.Controls.Add(this.panelLogin);
            this.Controls.Add(this.panelConnect);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "CityBike - Client App";
            this.panelConnect.ResumeLayout(false);
            this.panelConnect.PerformLayout();
            this.panelLogin.ResumeLayout(false);
            this.panelLogin.PerformLayout();
            this.panelOperations.ResumeLayout(false);
            this.panelOperations.PerformLayout();
            this.panelBikesList.ResumeLayout(false);
            this.panelConfirm.ResumeLayout(false);
            this.panelConfirm.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbLogin;
        private System.Windows.Forms.Label labelLoginError;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnReturnBike;
        private System.Windows.Forms.Button btnRentBike;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.Button btnChoseBike;
        private System.Windows.Forms.ListBox lbBikes;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Label labelUser;
        private System.Windows.Forms.Button btnCancel2;
        private System.Windows.Forms.Label labelLocationInfo;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelConfirm;
        private System.Windows.Forms.Label labelBikesLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panelConnect;
        private System.Windows.Forms.Panel panelLogin;
        private System.Windows.Forms.Panel panelOperations;
        private System.Windows.Forms.Panel panelBikesList;
        private System.Windows.Forms.Panel panelConfirm;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;

    }
}

