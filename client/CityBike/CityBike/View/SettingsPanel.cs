﻿using CityBike.Controler;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityBike.View
{
    public partial class SettingsPanel : Form
    {
        public SettingsPanel()
        {
            InitializeComponent();

            textBoxLogin.Text = ManageSettings.getUser();
            textBoxPassword.Text = ManageSettings.getPassword();
            textBoxServer.Text = ManageSettings.getUrl();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            ManageSettings.setPassword(textBoxPassword.Text);
            ManageSettings.setUrl(textBoxServer.Text);
            ManageSettings.setUser(textBoxLogin.Text);
            this.Close();
        }

        private void buttonTest_Click(object sender, EventArgs e)
        {

        }
    }
}
