﻿using CityBike.Controler;
using CityBike.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CityBike.View
{
    public partial class Form1 : Form
    {
        private List<Bike> myBikes = new List<Bike>();
        private User currentUser;
        private bool returningBike = false;
        private List<Bike> bikesList = new List<Bike>();
        private Bike selectedBike;
        private Status status;

        public Form1()
        {
            InitializeComponent();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new SettingsPanel().Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ExampleClass example = CityBikeClient.getExampleData();
                User user = CityBikeClient.getUser("user", "haslo");
                List<Bike> bikes = CityBikeClient.getMyBikes();
                Status rentStatus = CityBikeClient.rentBike(1, "admin");
                Status returnStatus = CityBikeClient.returnBike(1);
                MessageBox.Show("Ok!");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string username = tbLogin.Text;
            string password = tbPassword.Text;
            login(username, password);
        }

        private void connect()
        {
            try
            {
                myBikes = CityBikeClient.getMyBikes();
                menuStrip1.Enabled = false;
                labelLocationInfo.Text = ManageSettings.getUser();
                btnStart.Visible = false;
                logout();
            }
            catch (CityBikeClientException ex)
            {
                labelLocationInfo.Text = "Brak połączenia - " + ex.Message;
            }
            catch (Exception ex)
            {
                labelLocationInfo.Text = "Brak połączenia - niepoprawne ustawienia";
            }
        }

        private void login(string username, string password)
        {
            try
            {
                currentUser = CityBikeClient.getUser(username, password);
            }
            catch (Exception ex)
            {
                labelLoginError.Text = "Błąd połączenia!";
                currentUser = null;
            }

            if (currentUser != null && currentUser.valid == true)
            {
                showOperations();
                if (currentUser.employee == true)
                {
                    menuStrip1.Enabled = true;
                }
                btnLogout.Enabled = true;
            }
            else
            {
                labelLoginError.Text = "Niepoprawne dane logowania!";
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            connect();
        }


        private void logout()
        {
            labelLoginError.Text = "Wprowadź dane";
            menuStrip1.Enabled = false;
            btnLogout.Enabled = false;
            labelUser.Text = "Niezalogowany";
            currentUser = null;
            tbLogin.Text = "";
            tbPassword.Text = "";

            showPanel(0);
        }

        private void showOperations()
        {

            if (status != null)
            {

                if (status.ok)
                {
                    labelStatus.Text = "Operacja wykonana pomyślnie!";
                }
                else
                {
                    labelStatus.Text = "Błąd! Operacja nie została wykonana pomyślnie.";
                }
                status = null;
            }
            else
            {
                labelStatus.Text = "";
            }

            btnRentBike.Enabled = (currentUser.borrowAbility > 0 && myBikes.Count > 0) ? currentUser.enabled : false;
            btnReturnBike.Enabled = (currentUser.bikes.Count > 0 ) ? true : false;

            String s = currentUser.username;
            if (currentUser.employee)
                s += ", pracownik";
            if (currentUser.enabled == false)
                s += ", zablokowany";
            if (currentUser.borrowAbility <= 0)
                s += ", max rowerów";

            labelUser.Text = s;

            labelBikesLeft.Text = "Dostępnych rowerów : " + myBikes.Count;
            showPanel(1);
        }

        private void showBikesList()
        {
            bikesList.Clear();

            if (returningBike)
            {
                bikesList.AddRange(currentUser.bikes);
            }
            else
            {
                if (currentUser.employee)
                {
                    bikesList.AddRange(myBikes);
                }
                else
                {
                    bikesList.AddRange(myBikes.Where(b => b.blocked == false).ToList());
                }
            }

            lbBikes.DataSource = null;
            lbBikes.DataSource = bikesList;
            showPanel(2);
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            logout();
        }

        private void btnCancel2_Click(object sender, EventArgs e)
        {
            showOperations();
        }

        //wypozyczanie roweru
        private void rentBike_Click_1(object sender, EventArgs e)
        {
            returningBike = false;
            showBikesList();
        }


        private void button4_Click(object sender, EventArgs e)
        {
            showConfirmPage();
        }

        private void showConfirmPage()
        {
            string s = "";
            if (returningBike)
            {
                s += "Oddawanie roweru : ";
            }
            else
            {
                s += "Wypożyczanie roweru : ";
            }
            s += selectedBike.name;
            labelConfirm.Text = s;

            showPanel(3);
        }

        private void btnChoseBike_Click(object sender, EventArgs e)
        {
            selectedBike = (Bike)lbBikes.SelectedValue;
            showConfirmPage();
        }

        //oddawanie roweru
        private void btnReturnBike_Click(object sender, EventArgs e)
        {
            returningBike = true;
            showBikesList();
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (returningBike)
                returnBike();
            else
                rentBike();

            showStatus();
        }

        private void returnBike()
        {
            try
            {
                status = CityBikeClient.returnBike(selectedBike.id);
            }
            catch(Exception ex)
            {
                Status s = new Status();
                s.ok = false;
                status = s;
            }

            if (status.ok)
            {
                myBikes.Add(selectedBike);
                //currentUser.bikes.Remove(selectedBike);
                currentUser.bikes.RemoveAll(b => b.id == selectedBike.id);
                if (!currentUser.employee)
                    currentUser.borrowAbility++;
            }
        }

        private void rentBike()
        {
            try
            {
                status = CityBikeClient.rentBike(selectedBike.id, currentUser.username);
            }
            catch(Exception ex)
            {
                Status s = new Status();
                s.ok = false;
                status = s;
            }

            if (status.ok)
            {
                myBikes.RemoveAll(b => b.id == selectedBike.id);
                currentUser.bikes.Add(selectedBike);
                if (!currentUser.employee)
                    currentUser.borrowAbility--;
            }
        }

        private void showStatus()
        {
            showOperations();
        }

        private void btnCancel2_Click_1(object sender, EventArgs e)
        {
            showOperations();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            showOperations();
        }

        private void hideAllPanels()
        {
            panelBikesList.Visible = false;
            panelConfirm.Visible = false;
            panelConnect.Visible = false;
            panelLogin.Visible = false;
            panelOperations.Visible = false;
        }

        private void showPanel(int id)
        {
            hideAllPanels();
            Panel panel = null;
            switch (id)
            {
                case 0: panel = panelLogin; break;
                case 1: panel = panelOperations; break;
                case 2: panel = panelBikesList; break;
                case 3: panel = panelConfirm; break;
                default: break;
            }
            if (panel == null)
                logout();
            else
            { 
                panel.Visible = true;
                panel.Location = new Point(10, 30);
            }
        }

        private void btnSettings_Click(object sender, EventArgs e)
        {
            new SettingsPanel().Show();
        }
    }
}
