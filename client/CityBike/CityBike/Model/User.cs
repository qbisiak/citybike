﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityBike.Model
{
    class User
    {
        public bool valid { get; set; }
        public bool enabled { get; set; }
        public bool employee { get; set; }
        public int borrowAbility { get; set; }
        public string username { get; set; }
        public List<Bike> bikes { get; set; }
    }
}
