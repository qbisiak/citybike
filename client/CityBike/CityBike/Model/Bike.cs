﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityBike.Model
{
    class Bike
    {
        public int id { get; set; }
        public string name { get; set; }
        public bool blocked { get; set; }
        public string lockCode { get; set; }

        public override string ToString()
        {
            return this.name;
        }
    }
}
