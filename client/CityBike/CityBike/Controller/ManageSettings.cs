﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityBike.Controler
{
    class ManageSettings
    {
        public static string getUser()
        {
            return Properties.Settings.Default.user;
        }

        public static void setUser(string user)
        {
            Properties.Settings.Default.user = user;
        }

        public static string getUrl()
        {
            return Properties.Settings.Default.url;
        }

        public static void setUrl(string url)
        {
            Properties.Settings.Default.url = url;
        }

        public static string getPassword()
        {
            return StringCrypt.ToInsecureString(StringCrypt.DecryptString(Properties.Settings.Default.password));
        }

        public static void setPassword(string password)
        {
            Properties.Settings.Default.password = StringCrypt.EncryptString(StringCrypt.ToSecureString(password));
        }
    }
}
