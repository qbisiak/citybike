﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CityBike.Controler
{
    class CityBikeClientException : Exception
    {
        public CityBikeClientException(string message) : base(message)
        {

        }
    }
}
