﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using CityBike.Model;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Net;

namespace CityBike.Controler
{
   class CityBikeClient
    {

        public static T execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient(ManageSettings.getUrl());
            client.Authenticator = new HttpBasicAuthenticator(ManageSettings.getUser(), ManageSettings.getPassword());
            var response = client.Execute<T>(request);

            if (response.ErrorException != null)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.Unauthorized:
                        throw new CityBikeClientException("Niepoprawne dane logowania!");
                    case HttpStatusCode.Forbidden:
                        throw new CityBikeClientException("Zakaz dostępu!");
                    case HttpStatusCode.NotFound:
                        throw new CityBikeClientException("Nie znaleziono strony!");
                    default: break;

                }

                throw new CityBikeClientException(response.ErrorMessage);
            }
            return response.Data;
        }
        
        public static ExampleClass getExampleData()
        {
            var request = new RestRequest("restful/print");
            return execute<ExampleClass>(request);
        }

        public static User getUser(string login, string password)
        {
            var request = new RestRequest("restful/user-validation");
            request.Method = Method.POST;
            request.AddParameter("username", login);
            request.AddParameter("password", password);
            return execute<User>(request);
        }

        public static List<Bike> getMyBikes()
        {
            var request = new RestRequest("restful/my-bikes");
            return execute<List<Bike>>(request);
        }

        public static Status rentBike(long bikeId, string username)
        {
            var request = new RestRequest("restful/rent");
            request.Method = Method.POST;
            request.AddParameter("username", username);
            request.AddParameter("bike", bikeId);
            return execute<Status>(request);
        }

        public static Status returnBike(long bikeId)
        {
            var request = new RestRequest("restful/return");
            request.Method = Method.POST;
            request.AddParameter("bike", bikeId);
            return execute<Status>(request);
        }

    }
}
